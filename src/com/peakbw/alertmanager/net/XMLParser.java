package com.peakbw.alertmanager.net;

import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import com.peakbw.alertmanager.db.AlertContract.Alert;
import com.peakbw.alertmanager.db.AlertContract.CheckPoint;
import com.peakbw.alertmanager.db.AlertContract.Performance;
import com.peakbw.alertmanager.db.AlertContract.Signature;
import com.peakbw.alertmanager.db.AlertContract.Site;
import com.peakbw.alertmanager.db.AlertContract.Zone;
import com.peakbw.alertmanager.db.AlertDBHelper;
import com.peakbw.alertmanager.services.HeartBeat;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.Xml;

public class XMLParser {
	public static String status,method,signature,zoneid,confirmationNumber,checkpointid,zonename,siteid,sitename,supervisor,lastvisted,contact,performance,description,timestamp;
	private static final String ns = null;
	private static final String DEBUG_TAG = "XMLParser";
	private static SQLiteDatabase db;
	private static AlertDBHelper rDbHelper;
	private static ContentValues values = new ContentValues();
	
	public static String parseXML(InputStream is){
		
		rDbHelper = HeartBeat.rDbHelper;
        
        db = rDbHelper.getWritableDatabase();
		
		status = null;confirmationNumber=null;signature=null;siteid=null;lastvisted=null;performance=null;description=null;timestamp=null;sitename=null;checkpointid=null;
		try{
			XmlPullParser parser = Xml.newPullParser();
        	parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        	
        	int ch ;
            StringBuffer sb = new StringBuffer();
            while((ch = is.read())!=-1){
                sb.append((char)ch);
            }
            Log.d(DEBUG_TAG, "respone XML = : "+sb.toString());
            
        	if(is!=null){
        		parser.setInput(is, null);
        		parser.nextTag();
        		parser.require(XmlPullParser.START_TAG, ns, "reply");
        		
        		while (parser.next() != XmlPullParser.END_TAG) {
            		if (parser.getEventType() != XmlPullParser.START_TAG) {
            			continue;
            		}
            		String name = parser.getName();
            		Log.d(DEBUG_TAG, "TagName: "+name);
            		
            		if(name.equals("method")){
            			parser.require(XmlPullParser.START_TAG, ns, "method");
            			if (parser.next() == XmlPullParser.TEXT) {
            				method = (parser.getText()).substring(0,10);
            		        parser.nextTag();
            		    }
            			parser.require(XmlPullParser.END_TAG, ns, "method");
            			Log.d(DEBUG_TAG, "method: "+method);
            		}
            		else if(name.equals("signature")){
            			parser.require(XmlPullParser.START_TAG, ns, "signature");
            			if (parser.next() == XmlPullParser.TEXT) {
            				signature = parser.getText();
            				//update signature
            		        parser.nextTag();
            		    }
            			parser.require(XmlPullParser.END_TAG, ns, "signature");
            			Log.d(DEBUG_TAG, "signature: "+signature);
            		}
            		else if(name.equals("status")){
            			parser.require(XmlPullParser.START_TAG, ns, "status");
            			if (parser.next() == XmlPullParser.TEXT) {
            				status = parser.getText();
            				//update signature
            		        parser.nextTag();
            		    }
            			parser.require(XmlPullParser.END_TAG, ns, "status");
            			Log.d(DEBUG_TAG, "status: "+status);
            		}
            		else if(name.equals("confirmationNumber")){
            			parser.require(XmlPullParser.START_TAG, ns, "confirmationNumber");
            			if (parser.next() == XmlPullParser.TEXT) {
            				confirmationNumber = parser.getText();
            				//update signature
            		        parser.nextTag();
            		    }
            			parser.require(XmlPullParser.END_TAG, ns, "confirmationNumber");
            			Log.d(DEBUG_TAG, "confirmationNumber: "+confirmationNumber);
            		}
            		else if (name.equals("zones")) {
            			parser.require(XmlPullParser.START_TAG, ns, "zones");
            			while(parser.next()!= XmlPullParser.END_TAG){
    						if (parser.getEventType() != XmlPullParser.START_TAG) {
            	                continue;
            	            }
    						String name1 = parser.getName();
    						if (name1.equals("zone")) {
    							parser.require(XmlPullParser.START_TAG, ns, "zone");
    							while(parser.next()!= XmlPullParser.END_TAG){
    		        				if (parser.getEventType() != XmlPullParser.START_TAG) {
                    	                continue;
                    	            }
            						String name2 = parser.getName();
            						if(name2.equals("zoneid")) {
            							parser.require(XmlPullParser.START_TAG, ns, "zoneid");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				zoneid = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "zoneid");
            		        			Log.d(DEBUG_TAG, "zoneid: "+zoneid);
            						}
            						else if(name2.equals("zonename")){
            							parser.require(XmlPullParser.START_TAG, ns, "zonename");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				zonename = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "zonename");
            		        			Log.d(DEBUG_TAG, "zonename: "+zonename);
            						}
            						else{
            							skip(parser);
            						}
    							}
    							
    							values.put(Zone.ZONE_NAME, zonename.trim());
		        		    	values.put(Zone.ZONE_ID, zoneid.trim());
		        		    	
		        		    	// Insert the new row, returning the primary key value of the new row
		        		    	long newRowId = db.insertWithOnConflict(Zone.TABLE_NAME,Zone.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_IGNORE);
		        		    	Log.d(DEBUG_TAG, "New Zone Row Id:"+ newRowId+" "+zoneid);
		        		    	Log.d(DEBUG_TAG, "New Zone name: "+ zonename);
		        		    	values.clear();
    						}
    						else{
    							skip(parser);
    						}
            			}
            		}
            		else if(name.equals("sites")){
            			parser.require(XmlPullParser.START_TAG, ns, "sites");
            			while(parser.next()!= XmlPullParser.END_TAG){
    						if (parser.getEventType() != XmlPullParser.START_TAG) {
            	                continue;
            	            }
    						String name1 = parser.getName();
    						if (name1.equals("site")) {
    							parser.require(XmlPullParser.START_TAG, ns, "site");
    							while(parser.next()!= XmlPullParser.END_TAG){
    		        				if (parser.getEventType() != XmlPullParser.START_TAG) {
                    	                continue;
                    	            }
            						String name2 = parser.getName();
            						if(name2.equals("siteid")) {
            							parser.require(XmlPullParser.START_TAG, ns, "siteid");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				siteid = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "siteid");
            		        			Log.d(DEBUG_TAG, "siteid: "+siteid);
            						}
            						else if(name2.equals("zoneid")){
            							parser.require(XmlPullParser.START_TAG, ns, "zoneid");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				zoneid = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "zoneid");
            		        			Log.d(DEBUG_TAG, "zoneid: "+zoneid);
            						}
            						else if(name2.equals("sitename")){
            							parser.require(XmlPullParser.START_TAG, ns, "sitename");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				sitename = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "sitename");
            		        			Log.d(DEBUG_TAG, "sitename: "+sitename);
            						}
            						else if(name2.equals("supervisor")){
            							parser.require(XmlPullParser.START_TAG, ns, "supervisor");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				supervisor = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "supervisor");
            		        			Log.d(DEBUG_TAG, "supervisor: "+supervisor);
            						}
            						else if(name2.equals("contact")){
            							parser.require(XmlPullParser.START_TAG, ns, "contact");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				contact = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "contact");
            		        			Log.d(DEBUG_TAG, "contact: "+contact);
            						}
            						/*else if(name2.equals("performance")){
            							parser.require(XmlPullParser.START_TAG, ns, "performance");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				performance = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "performance");
            		        			Log.d(DEBUG_TAG, "performance: "+performance);
            						}*/
            						else{
            							skip(parser);
            						}
    							}
    							
    							values.put(Site.SITE_NAME, sitename.trim());
		        		    	values.put(Site.SITE_ID, siteid.trim());
		        		    	values.put(Site.ZONE_ID, zoneid.trim());
		        		    	values.put(Site.SUPERVISOR, supervisor.trim());
		        		    	values.put(Site.CONTACT, contact);
		        		    	//values.put(Site.PERFORMANCE, performance);
		        		    	//values.put(Site.TIMESTAMP, timestamp);
		        		    	
		        		    	// Insert the new row, returning the primary key value of the new row
		        		    	long newRowId = db.insertWithOnConflict(Site.TABLE_NAME,Site.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_IGNORE);
		        		    	Log.d(DEBUG_TAG, "New Site Row Id:"+ newRowId+" "+siteid);
		        		    	Log.d(DEBUG_TAG, "New Site name: "+ sitename);
		        		    	values.clear();
    						}
    						else{
    							skip(parser);
    						}
            			}
            		}
            		else if (name.equals("checkpoints")) {
            			parser.require(XmlPullParser.START_TAG, ns, "checkpoints");
            			while(parser.next()!= XmlPullParser.END_TAG){
    						if (parser.getEventType() != XmlPullParser.START_TAG) {
            	                continue;
            	            }
    						String name1 = parser.getName();
    						if (name1.equals("checkpoint")) {
    							parser.require(XmlPullParser.START_TAG, ns, "checkpoint");
    							while(parser.next()!= XmlPullParser.END_TAG){
    		        				if (parser.getEventType() != XmlPullParser.START_TAG) {
                    	                continue;
                    	            }
            						String name2 = parser.getName();
            						if(name2.equals("description")) {
            							parser.require(XmlPullParser.START_TAG, ns, "description");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				description = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "description");
            		        			Log.d(DEBUG_TAG, "description: "+description);
            						}
            						else if(name2.equals("lastvisted")){
            							parser.require(XmlPullParser.START_TAG, ns, "lastvisted");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				lastvisted = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "lastvisted");
            		        			Log.d(DEBUG_TAG, "lastvisted: "+lastvisted);
            						}
            						else if(name2.equals("checkpointid")){
            							parser.require(XmlPullParser.START_TAG, ns, "checkpointid");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				checkpointid = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "checkpointid");
            		        			Log.d(DEBUG_TAG, "checkpointid: "+checkpointid);
            						}
            						else if(name2.equals("siteid")){
            							parser.require(XmlPullParser.START_TAG, ns, "siteid");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				siteid = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "siteid");
            		        			Log.d(DEBUG_TAG, "siteid: "+siteid);
            						}
            						else{
            							skip(parser);
            						}
    							}
    							
    							values.put(CheckPoint.CHECK_POINT_NAME, description.trim());
		        		    	values.put(CheckPoint.CHECK_POINT_ID, checkpointid.trim());
		        		    	values.put(CheckPoint.SITE_ID, siteid.trim());
		        		    	values.put(CheckPoint.LASTVISTED, lastvisted.trim());
		        		    	
		        		    	// Insert the new row, returning the primary key value of the new row
		        		    	long newRowId = db.insertWithOnConflict(CheckPoint.TABLE_NAME,CheckPoint.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_IGNORE);
		        		    	Log.d(DEBUG_TAG, "New CheckPoint Row Id:"+ newRowId+" "+checkpointid);
		        		    	Log.d(DEBUG_TAG, "New CheckPoint: "+ description);
		        		    	values.clear();
    						}
    						else{
    							skip(parser);
    						}
            			}
            		}
            		else if (name.equals("alerts")) {
            			parser.require(XmlPullParser.START_TAG, ns, "alerts");
            			while(parser.next()!= XmlPullParser.END_TAG){
    						if (parser.getEventType() != XmlPullParser.START_TAG) {
            	                continue;
            	            }
    						String name1 = parser.getName();
    						if (name1.equals("alert")) {
    							parser.require(XmlPullParser.START_TAG, ns, "alert");
    							while(parser.next()!= XmlPullParser.END_TAG){
    		        				if (parser.getEventType() != XmlPullParser.START_TAG) {
                    	                continue;
                    	            }
            						String name2 = parser.getName();
            						if(name2.equals("description")) {
            							parser.require(XmlPullParser.START_TAG, ns, "description");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				description = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "description");
            		        			Log.d(DEBUG_TAG, "description: "+description);
            						}
            						else if(name2.equals("siteid")){
            							parser.require(XmlPullParser.START_TAG, ns, "siteid");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				siteid = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "siteid");
            		        			Log.d(DEBUG_TAG, "siteid: "+siteid);
            						}
            						else if(name2.equals("timestamp")){
            							parser.require(XmlPullParser.START_TAG, ns, "timestamp");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				timestamp = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "timestamp");
            		        			Log.d(DEBUG_TAG, "timestamp: "+timestamp);
            						}
            						else{
            							skip(parser);
            						}
    							}
    							
    							values.put(Alert.DESCRIPTION, description.trim());
		        		    	values.put(Alert.TIMESTAMP, timestamp.trim());
		        		    	values.put(Alert.SITE_ID, siteid.trim());
		        		    	
		        		    	// Insert the new row, returning the primary key value of the new row
		        		    	long newRowId = db.insertWithOnConflict(Alert.TABLE_NAME,Alert.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_IGNORE);
		        		    	Log.d(DEBUG_TAG, "New Alert Row Id:"+ newRowId+" "+siteid);
		        		    	Log.d(DEBUG_TAG, "New Alert: "+ description);
		        		    	values.clear();
    						}
    						else{
    							skip(parser);
    						}
            			}
            		}
            		else if (name.equals("performance")) {
            			parser.require(XmlPullParser.START_TAG, ns, "performance");
            			while(parser.next()!= XmlPullParser.END_TAG){
    						if (parser.getEventType() != XmlPullParser.START_TAG) {
            	                continue;
            	            }
    						String name1 = parser.getName();
    						if (name1.equals("record")) {
    							parser.require(XmlPullParser.START_TAG, ns, "record");
    							while(parser.next()!= XmlPullParser.END_TAG){
    		        				if (parser.getEventType() != XmlPullParser.START_TAG) {
                    	                continue;
                    	            }
            						String name2 = parser.getName();
            						if(name2.equals("siteid")) {
            							parser.require(XmlPullParser.START_TAG, ns, "siteid");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				siteid = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "siteid");
            		        			Log.d(DEBUG_TAG, "siteid: "+siteid);
            						}
            						else if(name2.equals("score")){
            							parser.require(XmlPullParser.START_TAG, ns, "score");
            		        			if (parser.next() == XmlPullParser.TEXT) {
            		        				performance = parser.getText();
            		        		        parser.nextTag();
            		        		    }
            		        			parser.require(XmlPullParser.END_TAG, ns, "score");
            		        			Log.d(DEBUG_TAG, "performance: "+performance);
            						}
            						else{
            							skip(parser);
            						}
    							}
    							
    							values.put(Performance.SCORE, performance.trim());
		        		    	values.put(Performance.SITE_ID, siteid.trim());
		        		    	
		        		    	// Insert the new row, returning the primary key value of the new row
		        		    	long newRowId = db.insertWithOnConflict(Performance.TABLE_NAME,Performance.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_REPLACE);
		        		    	Log.d(DEBUG_TAG, "New Performance Row Id:"+ newRowId+" "+siteid);
		        		    	Log.d(DEBUG_TAG, "New Performance: "+ description);
		        		    	values.clear();
    						}
    						else{
    							skip(parser);
    						}
            			}
            		}
            		else {
            			skip(parser);
            		}
        		}
        	}
        	
        	if(status!=null){
        		if(status.equalsIgnoreCase("SUCCESSFUL")){
        			updateSignature();
        		}
        	}
		}
		catch(IOException ex){
        	ex.printStackTrace();
        }
        catch(XmlPullParserException ex){
        	ex.printStackTrace();
        }
		
		return status;
	}

	private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                    depth--;
                    break;
            case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
	
	private static void updateSignature(){
		values.put(Signature.SIGNATURE, signature);
		values.put(Signature.DATE, timestamp);
		
		int count = db.update(
		    Signature.TABLE_NAME,
		    values,
		    null,
		    null);
		
		Log.d(DEBUG_TAG, "Update= : "+count);
		values.clear();
	}
}
