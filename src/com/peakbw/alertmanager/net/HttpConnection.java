package com.peakbw.alertmanager.net;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Date;

import android.util.Log;

public class HttpConnection {
	private static final String DEBUG_TAG = "HttpConnection";
	private String xmlstr;
	public static int respCode;
	public static String timeout;
	public void setUpdateVariable(String request){
		respCode = 0;
		timeout = null;
		this.xmlstr = request;
	}
	
	public InputStream makeConnection(String uri) throws IOException {
		Log.d(DEBUG_TAG, "URL = "+uri);
		InputStream stream = null;
		try{
			URL url = new URL(uri);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(150000 /* milliseconds */);
			conn.setConnectTimeout(20000 /* milliseconds */);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			OutputStream os = new BufferedOutputStream(conn.getOutputStream());
			Log.d(DEBUG_TAG, xmlstr);
			os.write(xmlstr.getBytes());
			os.flush();
			os.close();
			conn.connect();
			Log.d(DEBUG_TAG, "Connection open");
			respCode = conn.getResponseCode();
			if(respCode==200){
				//MainActivity.lenghtOfFile = conn.getContentLength();
				stream = new BufferedInputStream(conn.getInputStream());
				Log.d(DEBUG_TAG, "got inputstream");
				Log.d(DEBUG_TAG, String.valueOf(conn.getResponseCode()));
			}
        }
		catch(SocketTimeoutException ex){
			timeout = "Connection Timeout!";
		}
        return stream;
    }
	
	public static String getTrxnNumber() {
        String mnt = "";
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        String [] dateInfo = ((new Date()).toString()).split(" ");//Fri Nov 29 11:57:35 EAT 2013
        if(dateInfo[1].toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo[1].equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo[1].equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo[1].equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo[1].equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo[1].equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo[1].equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo[1].equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo[1].equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo[1].equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo[1].equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo[1].equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo[2]+mnt+(dateInfo[5].substring(2, 4))+(dateInfo[3].substring(0, 2))+(dateInfo[3].substring(3, 5))+(dateInfo[3].substring(6, 8));
        return txn;
    }
}
