package com.peakbw.alertmanager.services;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.peakbw.alertmanager.activities.MainActivity;
import com.peakbw.alertmanager.activities.R;
import com.peakbw.alertmanager.db.AlertContract.Signature;
import com.peakbw.alertmanager.db.AlertDBHelper;
import com.peakbw.alertmanager.net.XMLParser;

public class HeartBeat extends Service {
	public static String iccid,imei,/*batteryLevel,*/signature;
	//private boolean isCharging,usbCharge,acCharge;
	private static final String DEBUG_TAG ="HeartBeatService";
	private Timer startTimer;
	public static AlertDBHelper rDbHelper;
	public static String uri = "http://www.peakbw.com/aandm/backend/mobile/mobile.php";
	//public static String uri = "http://10.0.2.2:8084/MarcsWeb/Tolls";
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
    public void onCreate() {
          super.onCreate();
        //get phone unique identifier 
          TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
          imei = telephonyManager.getDeviceId();
          Log.d(DEBUG_TAG, imei);
          
          TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
          iccid = telemamanger.getSimSerialNumber();
          Log.d(DEBUG_TAG, iccid);
          
          //IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
          //Intent batteryStatus = this.registerReceiver(null, ifilter);
          
       // Are we charging / charged?
          //int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
          //isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||status == BatteryManager.BATTERY_STATUS_FULL;

          // How are we charging?
          //int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
          //usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
          //acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
          
          //int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
          //int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

          //float batteryPct = level / (float)scale;
          //batteryLevel = String.valueOf(batteryPct);
          
          rDbHelper = new AlertDBHelper(getBaseContext());
          
          startSheduler();
          
         signature = getSignature();
    }
	
	private void startSheduler() {
		startTimer = new Timer();
		try {
			// thread startd up  on boot
			startTimer.scheduleAtFixedRate(new StartTask(),0, 1 * 60000);
			// every after 1 min (60000) send request
		}
		catch (Exception ex) {
			
		}
	}
	
	class StartTask extends TimerTask {
		int count = 0;
		public void run() {
			count++;
			Log.d(DEBUG_TAG, "count "+count);
			if(count == 10){
				count =0;
				makeConnection();
			}
		}
	}
	
	private void makeConnection(){
		int respCode = 0;
		InputStream stream = null;
		String xmlstr = createXML();
		try{//
			URL url = new URL(uri);
			Log.d(DEBUG_TAG, "url = "+url);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			Log.d(DEBUG_TAG, "connection created");
			conn.setReadTimeout(150000 );
			Log.d(DEBUG_TAG, "read time out set");
			conn.setConnectTimeout(20000 );
			Log.d(DEBUG_TAG, "conn timeout set");
			conn.setRequestMethod("POST");
			Log.d(DEBUG_TAG, "POST method set");
			conn.setDoInput(true);
			Log.d(DEBUG_TAG, "doInput = true");
			conn.setDoOutput(true);
			Log.d(DEBUG_TAG, "doOutput = true");
			OutputStream os = new BufferedOutputStream(conn.getOutputStream());
			Log.d(DEBUG_TAG, "XML = "+xmlstr);
			os.write(xmlstr.getBytes());
			os.flush();
			os.close();
			conn.connect();
			Log.d(DEBUG_TAG, "Connection open");
			respCode = conn.getResponseCode();
			Log.d(DEBUG_TAG, "RespCode = "+respCode);
			if(respCode==200){
				//MainActivity.lenghtOfFile = conn.getContentLength();
				stream = new BufferedInputStream(conn.getInputStream());
				Log.d(DEBUG_TAG, "got inputstream");
				
				/*int ch ;
		        StringBuffer sb = new StringBuffer();
		        while((ch = stream.read())!=-1){
		            sb.append((char)ch);
		        }
		        Log.d(DEBUG_TAG, "Reply XML= "+sb.toString());*/
				
				notifyUser(XMLParser.parseXML(stream));
			}
        }
		catch(IOException ex){
			ex.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private String createXML(){
		StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        xmlStr.append("<method>").append("askari_alert").append("</method>");
        xmlStr.append("<imei>").append("354802065531601").append("</imei>");
        xmlStr.append("<iccid>").append(iccid.substring(0, iccid.length()-1)).append("</iccid>");
        xmlStr.append("<signature>").append(getSignature()).append("</signature>");
        xmlStr.append("<pin>").append(9990).append("</pin>");
        //xmlStr.append("<usbCharge>").append(usbCharge).append("</usbCharge>");
        //xmlStr.append("<acCharge>").append(acCharge).append("</acCharge>");
        //xmlStr.append("<batteryLevel>").append(batteryLevel).append("</batteryLevel>");
        xmlStr.append("</pbtRequest>");
        Log.d(DEBUG_TAG, "Request = "+xmlStr.toString());
        return xmlStr.toString();
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@SuppressLint("NewApi")
	private void notifyUser(String status){
		int notifyID = 1,numMessages = 0;
		String currentText = "You have Received new Alerts!";
		Log.d(DEBUG_TAG, "Status = "+status);
		
		Uri.parse("android.resource://"+getPackageName() + "/" + R.raw.siren);
		
		//if(status!=null){
			//if(status.equals("SUCCESSFUL")){
				
				/*Intent intent = new Intent(getBaseContext(),MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);*/
				
				NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
				        .setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle("New Alert")
				        .setContentText("You have Received new Alerts!");
				
				Intent resultIntent = new Intent(this, MainActivity.class);
				TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
				stackBuilder.addParentStack(MainActivity.class);
				
				stackBuilder.addNextIntent(resultIntent);
				PendingIntent resultPendingIntent =stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
				
				mBuilder.setContentIntent(resultPendingIntent);
				
				//Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				Uri alarmSound = Uri.parse("android.resource://"+getPackageName() + "/" + R.raw.siren);
				
				mBuilder.setSound(alarmSound);
				
				mBuilder.setAutoCancel(true);
				
				NotificationManager mNotificationManager =
					    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
				
				mBuilder.setContentText(currentText).setNumber(++numMessages);
				
				mNotificationManager.notify(notifyID, mBuilder.build());
			//}
		//}
	}
	
	public static String getSignature(){
		Cursor cursor = rDbHelper.getSignature();
		Log.d(DEBUG_TAG, "sign count = "+cursor.getCount());
        cursor.moveToFirst();
        return cursor.getString(cursor.getColumnIndexOrThrow(Signature.SIGNATURE));
	}
	
}
