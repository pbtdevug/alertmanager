package com.peakbw.alertmanager.activities;

import java.io.IOException;
import java.io.InputStream;
import com.peakbw.alertmanager.db.AlertContract.Alert;
import com.peakbw.alertmanager.db.AlertContract.Performance;
import com.peakbw.alertmanager.net.HttpConnection;
import com.peakbw.alertmanager.net.XMLParser;
import com.peakbw.alertmanager.services.HeartBeat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class ClaerActivity extends Activity {
	private static final String DEBUG_TAG = "ClaerActivity";
	//private TextView label;
	private ProgressBar  pb;
	private EditText et,pe;
	private HttpConnection hc;
	private String msg,pin;
	//private Button sub;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_claer);
		// Show the Up button in the action bar.
		setupActionBar();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		//label = (TextView) findViewById(R.id.textView1);
		pb = (ProgressBar) findViewById(R.id.progressBar1);
		et = (EditText) findViewById(R.id.multiAutoCompleteTextView1);
		pe = (EditText) findViewById(R.id.user_pini);
		//sub = (Button) findViewById(R.id.button1);
		
		hc = new HttpConnection();
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.claer, menu);
		return true;
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void clickListener(View view){
		pin = pe.getText().toString();
		pe.setText("");
		
		msg = et.getText().toString();
		et.setText("");
		
		if(pin.length()==4){
			if(msg.length()>1){
				pb.setVisibility(View.VISIBLE);
				//delay();	
				
				hc.setUpdateVariable(createRequestXML());
				new DownloadXMLTask().execute(HeartBeat.uri);
			}
			else{
	    		Toast.makeText(this,"Enter remarks", Toast.LENGTH_LONG).show();
	    	}
		}
		else{
    		Toast.makeText(this,"PIN must be 4 digits!", Toast.LENGTH_LONG).show();
    	}
	}
	
	private String createRequestXML(){
		StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        xmlStr.append("<method>").append("askari_alert_clear").append("</method>");
        xmlStr.append("<imei>").append("354802065531601"/*HeartBeat.imei*/).append("</imei>");
        xmlStr.append("<iccid>").append(HeartBeat.iccid).append("</iccid>");
        xmlStr.append("<signature>").append(HeartBeat.getSignature()).append("</signature>");
        xmlStr.append("<timestamp>").append(DetailsActivity.timeStamp).append("</timestamp>");
        xmlStr.append("<siteid>").append(MainActivity.selectedId).append("</siteid>");
        xmlStr.append("<remarks>").append(msg).append("</remarks>");
        xmlStr.append("<transactionNumber>").append(HttpConnection.getTrxnNumber()).append("</transactionNumber>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("</pbtRequest>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
	
	public class DownloadXMLTask extends AsyncTask<String, Void, String> {
		private String status = null;
		private InputStream stream = null;
		private static final String DEBUG_TAG = "DownloadXMLTask";

		@Override
		protected String doInBackground(String... url) {
			try {
	    		try	{
	    			status = XMLParser.parseXML(hc.makeConnection(url[0]));
	    		}
	    		catch(Exception ex){
	    			ex.printStackTrace();
	    		}
	    	}
	    	catch(Exception ex){
	        	ex.printStackTrace();
	        }
	        finally {
	            if (stream != null) {
	            	try{
	            		stream.close();
	                }catch(IOException ex){
	                	
	                }
	            }
	        }
	    	return status;
		}
		
		@Override
	    protected void onPostExecute(String result) {
	    	if(result!=null){
	    		Log.d(DEBUG_TAG, result.toString());
	    		//et.setVisibility(View.INVISIBLE);
	    		//pe.setVisibility(View.INVISIBLE);
	    		//sub.setVisibility(View.INVISIBLE);
	    		if(result.equals("SUCCESSFUL")){
	    			createDialog(result,R.layout.success_prompt,R.id.success);
	    			updateAlerts(DetailsActivity.alertID,MainActivity.selectedId);
	    		}
	    		else{
	    			createDialog(result,R.layout.error_prompt,R.id.error);
	    		}
	    	}
	    	else{
	    		createDialog("Connection Timeout",R.layout.error_prompt,R.id.error);
	    	}	
	    }
	}
	
	private int updateAlerts(String alertID,String siteId){
		SQLiteDatabase db = HeartBeat.rDbHelper.getReadableDatabase();
		
		ContentValues values = new ContentValues();
    	values.put(Alert.CLAERED, true);
    	
    	String selection = Alert._ID + " = ?";
    	
    	String whereClause = Performance.SITE_ID + " = ?";
    	
    	String[] selectionArgs = {alertID};
    	
    	String[] whereArgs = {siteId};

    	int count = db.update(Alert.TABLE_NAME,values,selection,selectionArgs);
    	
    	db.delete(Performance.TABLE_NAME, whereClause, whereArgs);
    	
    	values.clear();
    	Log.d(DEBUG_TAG, "Affected Row ID = "+count);
    	return count;
	}
	
	@SuppressLint("InflateParams")
	private void createDialog(String msg,int layout,int textView){
    	LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(layout, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		
		final TextView successTextView = (TextView) promptsView.findViewById(textView);
		successTextView.setText(msg);
		
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	startMain();
			    }
			  });
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
    }
	
	private void startMain(){
		DetailsActivity.getInstance().finish();
		finish();
	}

}
