package com.peakbw.alertmanager.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.util.Linkify;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.peakbw.alertmanager.db.AlertContract.Alert;
import com.peakbw.alertmanager.db.AlertContract.Site;
import com.peakbw.alertmanager.services.HeartBeat;

public class DetailsActivity extends Activity {
	
	private static DetailsActivity da;
	private TextView siteTex,inchargeText,checkPtText,head;
	public static String timeStamp,alertID;
	private static final String DEBUG_TAG = "DetailsActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		// Show the Up button in the action bar.
		setupActionBar();
		
		siteTex = (TextView)findViewById(R.id.siteName);
		inchargeText = (TextView)findViewById(R.id.person);
		inchargeText.setAutoLinkMask(Linkify.PHONE_NUMBERS);
		checkPtText = (TextView)findViewById(R.id.checkPtName);
		head = (TextView)findViewById(R.id.heading);
		
		da = this;
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		Cursor cursor = HeartBeat.rDbHelper.getAlerts(MainActivity.selectedId);
		
		String site ="",description="",name="",contact="";
		
		while(cursor.moveToNext()){
			site = cursor.getString(cursor.getColumnIndexOrThrow(Site.SITE_NAME));
			description = cursor.getString(cursor.getColumnIndexOrThrow(Alert.DESCRIPTION));
        	name = cursor.getString(cursor.getColumnIndexOrThrow(Site.SUPERVISOR));
        	contact= cursor.getString(cursor.getColumnIndexOrThrow(Site.CONTACT));
        	timeStamp = cursor.getString(cursor.getColumnIndexOrThrow(Alert.TIMESTAMP));
        	alertID = cursor.getString(cursor.getColumnIndexOrThrow(Alert._ID));
        	//String cleared = cursor.getString(cursor.getColumnIndexOrThrow(Alert.CLAERED));
        	Log.d(DEBUG_TAG, "AlertID = "+alertID);
		}
		
		cursor.close();
		
		siteTex.setText(site);
		inchargeText.setText(name+", "+contact);
		checkPtText.setText(description);
		head.setText("Patrol Exceptions as at "+timeStamp);
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.details, menu);
		return true;
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onClickListener(View view){
		Intent intent = new Intent(this,ClaerActivity.class);
		startActivity(intent);
	}
	
	public static DetailsActivity getInstance(){
		return da;
	}
	
	public void makeCall(View view){
		Log.d(DEBUG_TAG, "make call");
	}

}
