package com.peakbw.alertmanager.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.peakbw.alertmanager.db.AlertContract.Performance;
import com.peakbw.alertmanager.db.AlertContract.Site;
import com.peakbw.alertmanager.db.AlertDBHelper;

public class MainActivity extends ListActivity {
	
	public static String selectedId;
	private SimpleAdapter adapter;
	private static final String DEBUG_TAG = "MainActivity";
	private Hashtable<String,String> site_id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//Intent serviceIntent = new Intent(this, HeartBeat.class);
        //startService(serviceIntent);
        Log.d(DEBUG_TAG, "Activity Created");
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		Log.d(DEBUG_TAG, "Activity Started");
		
		site_id = new Hashtable<String,String>();
		
		Cursor cursor = (new AlertDBHelper(getBaseContext()))/*HeartBeat.rDbHelper*/.getSites();
		List<Map<String, String>> sitesList = new ArrayList<Map<String, String>>(2);
		while(cursor.moveToNext()){
			Map<String, String> datum = new HashMap<String, String>(2);
			
			String name = cursor.getString(cursor.getColumnIndexOrThrow(Site.SITE_NAME));
        	String id = cursor.getString(cursor.getColumnIndexOrThrow(Site.SITE_ID));
        	String supervisor = cursor.getString(cursor.getColumnIndexOrThrow(Site.SUPERVISOR));
        	String performance = cursor.getString(cursor.getColumnIndexOrThrow(Performance.SCORE));
        	
        	site_id.put(name, id);
        	
        	Log.d(DEBUG_TAG, "Site: "+name);
        	
        	datum.put("site", name);
            datum.put("perf", performance+"%");
            datum.put("super", supervisor);
            
            sitesList.add(datum);
            
		}
		
		adapter = new SimpleAdapter(this, sitesList,R.layout.list_elements,
                new String[] {"site", "super","perf"},new int[] {R.id.item,R.id.center_item,R.id.right_item});
		
		setListAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override 
	public void onListItemClick(ListView l, View v, int position, long id) {
		@SuppressWarnings("unchecked")
		HashMap<String, String> histMap = (HashMap<String, String>)l.getItemAtPosition(position);
		String selectedHist = histMap.get("site");
		
		selectedId = site_id.get(selectedHist).toString();
		
		System.out.println(selectedId);
		
		Intent inten = new Intent(this,DetailsActivity.class);
		startActivity(inten);
	}

}
