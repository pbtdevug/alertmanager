package com.peakbw.alertmanager.db;

import android.provider.BaseColumns;

public final class AlertContract {
	
	public static abstract class Zone implements BaseColumns {
        public static final String TABLE_NAME = "zone";
        public static final String ZONE_NAME = "name";
        public static final String ZONE_ID = "id";
        public static final String COLUMN_NAME_NULLABLE = "null";
	}
	
	public static abstract class Site implements BaseColumns {
        public static final String TABLE_NAME = "site";
        public static final String SITE_NAME = "name";
        public static final String SITE_ID = "id";
        public static final String ZONE_ID = "zoneid";
        public static final String SUPERVISOR = "supervisor";
        public static final String CONTACT = "contact";
        //public static final String PERFORMANCE = "performance";
        //public static final String TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_NULLABLE = "null";
	}
	
	public static abstract class CheckPoint implements BaseColumns {
        public static final String TABLE_NAME = "checkpoint";
        public static final String CHECK_POINT_NAME = "name";
        public static final String CHECK_POINT_ID = "id";
        public static final String SITE_ID = "siteid";
        public static final String LASTVISTED = "lastvisted";
        public static final String COLUMN_NAME_NULLABLE = "null";
	}
	
	public static abstract class Alert implements BaseColumns {
        public static final String TABLE_NAME = "alerts";
        public static final String DESCRIPTION = "description";
        public static final String TIMESTAMP = "timestamp";
        public static final String SITE_ID = "siteid";
        public static final String CLAERED = "cleared";
        public static final String COLUMN_NAME_NULLABLE = "null";
	}
	
	public static abstract class Signature implements BaseColumns {
        public static final String TABLE_NAME = "signature";
        public static final String SIGNATURE = "updateSignature";
        public static final String DATE= "date";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }
	
	public static abstract class Performance implements BaseColumns {
        public static final String TABLE_NAME = "performance";
        public static final String SITE_ID = "siteid";
        public static final String SCORE= "score";
        //public static final String TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_NULLABLE = "null";
    } 
}
