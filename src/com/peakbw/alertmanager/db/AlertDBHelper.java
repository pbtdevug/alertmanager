package com.peakbw.alertmanager.db;

import java.util.Date;
import com.peakbw.alertmanager.db.AlertContract.Alert;
import com.peakbw.alertmanager.db.AlertContract.CheckPoint;
import com.peakbw.alertmanager.db.AlertContract.Performance;
import com.peakbw.alertmanager.db.AlertContract.Signature;
import com.peakbw.alertmanager.db.AlertContract.Site;
import com.peakbw.alertmanager.db.AlertContract.Zone;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AlertDBHelper extends SQLiteOpenHelper {
	public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "paygatedatabase.db";
    public static SQLiteDatabase mDB;
    public static final String DEBUG_TAG = "PayGateDBHelper";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static ContentValues values = new ContentValues();
    
    private static final String SQL_CREATE_ZONE =
    	    "CREATE TABLE " + Zone.TABLE_NAME + " (" +
    	    		Zone._ID + " INTEGER PRIMARY KEY," + 
    	    		Zone.ZONE_ID +" INTEGER UNIQUE NOT NULL," +
    	    		Zone.ZONE_NAME + TEXT_TYPE + ")";
    
    private static final String SQL_DELETE_ZONE ="DROP TABLE IF EXISTS " + Zone.TABLE_NAME;
    
    private static final String SQL_CREATE_SITE =
    	    "CREATE TABLE " + Site.TABLE_NAME + " (" +
    	    		Site._ID + " INTEGER PRIMARY KEY," + 
    	    		Site.SITE_ID + " INTEGER UNIQUE NOT NULL," +
    	    		Site.ZONE_ID +" INTEGER ," +
    	    		Site.SUPERVISOR + TEXT_TYPE + COMMA_SEP +
    	    		Site.CONTACT + TEXT_TYPE + COMMA_SEP +
    	    		//Site.PERFORMANCE + " INTEGER ," +
    	    		//Site.TIMESTAMP + TEXT_TYPE + COMMA_SEP +
    	    		Site.SITE_NAME + TEXT_TYPE + ")";
    
    private static final String SQL_DELETE_SITE ="DROP TABLE IF EXISTS " + Site.TABLE_NAME;
    
    private static final String SQL_CREATE_CHECKPOINT =
    	    "CREATE TABLE " + CheckPoint.TABLE_NAME + " (" +
    	    		CheckPoint._ID + " INTEGER PRIMARY KEY," + 
    	    		CheckPoint.CHECK_POINT_ID +" INTEGER UNIQUE NOT NULL," +
    	    		CheckPoint.SITE_ID +" INTEGER ," +
    	    		CheckPoint.LASTVISTED + TEXT_TYPE + COMMA_SEP +
    	    		CheckPoint.CHECK_POINT_NAME + TEXT_TYPE + ")";
    
    private static final String SQL_DELETE_CHECKPOINT ="DROP TABLE IF EXISTS " + CheckPoint.TABLE_NAME;
    
    private static final String SQL_CREATE_ALERT =
    	    "CREATE TABLE " + Alert.TABLE_NAME + " (" +
    	    		Alert._ID + " INTEGER PRIMARY KEY," + 
    	    		Alert.SITE_ID +" INTEGER ," +
    	    		Alert.DESCRIPTION + TEXT_TYPE + COMMA_SEP +
    	    		Alert.CLAERED + " BOOLEAN DEFAULT FALSE" + COMMA_SEP +
    	    		Alert.TIMESTAMP + TEXT_TYPE + ")";
    
    private static final String SQL_DELETE_ALERT ="DROP TABLE IF EXISTS " + Alert.TABLE_NAME;
    
    private static final String SQL_CREATE_SIGNATURE =
    	    "CREATE TABLE " + Signature.TABLE_NAME + " (" +
    	    		Signature._ID + " INTEGER PRIMARY KEY," + 
    	    		Signature.SIGNATURE + TEXT_TYPE + COMMA_SEP +
    	    		Signature.DATE + TEXT_TYPE + ")";
    
    private static final String SQL_DELETE_SIGNATURE ="DROP TABLE IF EXISTS " + Signature.TABLE_NAME;
    
    private static final String SQL_CREATE_PERFORMANCE =
    	    "CREATE TABLE " + Performance.TABLE_NAME + " (" +
    	    		Performance._ID + " INTEGER PRIMARY KEY," + 
    	    		Performance.SITE_ID + " INTEGER UNIQUE NOT NULL " + COMMA_SEP +
    	    		Performance.SCORE + " INTEGER )";
    
    private static final String SQL_DELETE_PERFORMANCE ="DROP TABLE IF EXISTS " + Performance.TABLE_NAME;
    
    public AlertDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		AlertDBHelper.mDB = getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_ZONE);
		Log.d(DEBUG_TAG, "SQL for ZONE:"+ SQL_CREATE_ZONE);
		db.execSQL(SQL_CREATE_SITE);
		Log.d(DEBUG_TAG, "SQL for SITE:"+ SQL_CREATE_SITE);
		db.execSQL(SQL_CREATE_ALERT);
		Log.d(DEBUG_TAG, "SQL for ALERT:"+ SQL_CREATE_ALERT);
		db.execSQL(SQL_CREATE_CHECKPOINT);
		Log.d(DEBUG_TAG, "SQL for CHECKPOINT:"+ SQL_CREATE_CHECKPOINT);
		db.execSQL(SQL_CREATE_SIGNATURE);
		Log.d(DEBUG_TAG, "SQL for CHECKPOINT:"+ SQL_CREATE_SIGNATURE);
		db.execSQL(SQL_CREATE_PERFORMANCE);
		Log.d(DEBUG_TAG, "SQL for PERFORMANCE:"+ SQL_CREATE_PERFORMANCE);
		//initial signature
		insertFirstSignature(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_ZONE);
		db.execSQL(SQL_DELETE_SITE);
		db.execSQL(SQL_DELETE_ALERT);
		db.execSQL(SQL_DELETE_CHECKPOINT);
		db.execSQL(SQL_DELETE_SIGNATURE);
		db.execSQL(SQL_DELETE_PERFORMANCE);
	}
	
	private void insertFirstSignature(SQLiteDatabase db){
    	values.put(Signature.SIGNATURE, "0");
    	values.put(Signature.DATE, (new Date()).toString());
    	
    	// Insert the new row, returning the primary key value of the new row
    	long newRowId = db.insertWithOnConflict(
    			Signature.TABLE_NAME,
    			Signature.COLUMN_NAME_NULLABLE,
    			values,
    			SQLiteDatabase.CONFLICT_REPLACE);
    	Log.d(DEBUG_TAG, "New Signature Row Id:"+ newRowId+" "+"0");
    	Log.d(DEBUG_TAG, "New Signature: "+ "Signature"+"0");
    	values.clear();
    }
	
	public Cursor getSignature(){
    	Cursor c = mDB.query(Signature.TABLE_NAME, new String[] {Signature._ID, Signature.SIGNATURE} ,
    			null, null, null, null,null);
    	return c;
    }
	
	public Cursor getSites(){
		Log.d(DEBUG_TAG, "getSites Invoked");
    	Cursor c = mDB.rawQuery("SELECT s.name, s.supervisor,s.id,p.score FROM site s JOIN performance p ON p.siteid = s.id;", null);
    	return c;
    }
	
	public Cursor getAlerts(String siteId){
    	Cursor c = mDB.rawQuery("SELECT s.name, s.supervisor, s.id, s.contact, a.description, a.timestamp,a._ID FROM site s JOIN alerts a ON a.siteid = s.id WHERE s.id = ? AND a.cleared = ?;", new String[]{siteId,"FALSE"});
    	return c;
    }

}
